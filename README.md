#This was a personal project to complete the task.

The Task is to Build-Menu from 
backend side using laravel and vue, where we have category,sub category,items and discounts.

Building Menu: Admin can create categories, subcategories and items for his menu.
○ The Maximum level of subcategories is four.
○ Category or subcategory must not have mixed children (their children type at
any level are only subcategories or items).

2. Discounts: Restaurant admin can create discounts for his menu.
○ Discount types:
■ All Menu Discount
■ Category/Subcategory Discount
■ Item Discount
○ Items and subcategories can inherit the closest discount if they don’t have one.
○ Menu should be returned from the backend with computed discounts.

what tasks I have made?

1- Category or subcategory must not have mixed children (their children type atany level are only subcategories or items

2- creating menus

3- used laravel action.

4- debug the code and solved some bugs when creating or updating categories.


#Basic Installation Steps of the Project

Before you start the installation process you need to have installed composer package

Clone the project

Navigate to the project root directory using command line

Run bash composer install, npm install

Copy .env.example into .env file

Adjust DB_* parameters.

If you want to use Mysql database, make sure that you have mysql server up and running.

Run bash php artisan migrate

Run php artisan serve which will start local server

Run npm run dev which will start vite